# Increase log verbosity
log_level = "DEBUG"

#bind_addr = "193.146.75.100"
#If bind_addr does not work, use advertise
advertise{
http = "193.146.75.100:4646"
rpc = "193.146.75.100:4647"
serf = "193.146.75.100:4648"
}

# Setup data dir
data_dir = "/opt/nomad"

# Enable the server
server {
  enabled = true
  bootstrap_expect = 5
}
# Require TLS
tls {
  http = true
  rpc  = true

  ca_file   = "/home/ubuntu/nomad-ca.pem"
  cert_file = "/home/ubuntu/server.pem"
  key_file  = "/home/ubuntu/server-key.pem"

  verify_server_hostname = true
  verify_https_client    = true
  # ...
}

