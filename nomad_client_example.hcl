# Increase log verbosity
log_level = "DEBUG"

# Setup data dir
data_dir = "/opt/nomad"

# Enable the client
client {
  enabled = true
}
  # For demo assume you are talking to server1. For production,
  # this should be like "nomad.service.consul:4647" and a system
  # like Consul used for service discovery.

# Modify our port to avoid a collision with server1

# Require TLS
tls {
  http = true
  rpc  = true

  ca_file   = "/home/ubuntu/nomad-ca.pem"
  cert_file = "/home/ubuntu/server.pem"
  key_file  = "/home/ubuntu/server-key.pem"

  verify_server_hostname = true
  verify_https_client    = true
}
