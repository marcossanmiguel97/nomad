# NOMAD



## Nomad cluster deployment

Guide:

https://learn.hashicorp.com/tutorials/nomad/production-deployment-guide-vm-with-consul

https://learn.hashicorp.com/tutorials/nomad/security-enable-tls


![alt text](/nomad_architecture.png)



## Install Nomad

```
export NOMAD_VERSION="1.1.0"
```
```
curl --silent --remote-name https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip
```
```
unzip nomad_${NOMAD_VERSION}_linux_amd64.zip
```
```
sudo chown root:root nomad
```
```
sudo mv nomad /usr/local/bin/
```
```
nomad version
```
The nomad command features opt-in autocompletion for flags, subcommands, and arguments (where supported). Enable autocompletion.
```
nomad -autocomplete-install
```

```
complete -C /usr/local/bin/nomad nomad
```
Create a data directory for Nomad.
```
sudo mkdir --parents /opt/nomad
```
```
sudo useradd --system --home /etc/nomad.d --shell /bin/false nomad
```

## Certificate Authority
```
cfssl print-defaults csr | cfssl gencert -initca - | cfssljson -bare nomad-ca
```
The CA key (nomad-ca-key.pem) will be used to sign certificates for Nomad nodes and must be kept private. The CA certificate (nomad-ca.pem) contains the public key necessary to validate Nomad certificates and therefore must be distributed to every node that requires access.
## Node Certificates
To create certificates for the client and server in the cluster from the Nomad GitHub repository with cfssl create (or download) the following configuration file as cfssl.json to increase the default certificate expiration time:
```
{
  "signing": {
    "default": {
      "expiry": "87600h",
      "usages": ["signing", "key encipherment", "server auth", "client auth"]
    }
  }
}
```
Generate a certificate for the Nomad server. For this example, publicip host is used for the certificate. Thus, publicip must be added to /ets/hosts with its corresponding <public_ip> where nomad server is running.

```
echo '{}' | cfssl gencert -ca=nomad-ca.pem -ca-key=nomad-ca-key.pem -config=cfssl.json \
    -hostname="publicip, server.global.nomad,localhost,127.0.0.1" - | cfssljson -bare server

```
Generate a certificate for the Nomad client.
```
echo '{}' | cfssl gencert -ca=nomad-ca.pem -ca-key=nomad-ca-key.pem -config=cfssl.json \
    -hostname="publicip, client.global.nomad,localhost,127.0.0.1" - | cfssljson -bare client

```
Generate a certificate for the CLI.
```
echo '{}' | cfssl gencert -ca=nomad-ca.pem -ca-key=nomad-ca-key.pem -profile=client \
    - | cfssljson -bare cli
```
Using localhost and 127.0.0.1 as subject alternate names (SANs) allows tools like curl to be able to communicate with Nomad's HTTP API when run on the same host. Other SANs may be added including a DNS resolvable hostname to allow remote HTTP requests from third party tools.

Share nomad-ca.pem, cli.pem and cli-key.pem to acces nomad cli remotely.
## Configure Nomad agents
Create a data directory for Nomad.
```
sudo mkdir --parents /opt/nomad
```
Create a unique, non-privileged system user to run Nomad.
```
sudo useradd --system --home /etc/nomad.d --shell /bin/false nomad
```
Check example config file in nomad_example.hcl.
```
sudo mkdir --parents /etc/nomad.d
```
```
sudo chmod 700 /etc/nomad.d
```
```
sudo touch /etc/nomad.d/nomad.hcl
```

## Server specific configuration
Check example config file in nomad_server_example.hcl.
```
sudo touch /etc/nomad.d/server.hcl
```
Files needed:
```
  ca_file   = "nomad-ca.pem"
  cert_file = "server.pem"
  key_file  = "server-key.pem"
  ```
## Client specific configuration
Check example config file in nomad_client_example.hcl.
```
sudo touch /etc/nomad.d/client.hcl
```
Files needed:
```
  ca_file   = "nomad-ca.pem"
  cert_file = "client.pem"
  key_file  = "client-key.pem"
  ```
## Configure consul connect 
### CNI Plugins

Nomad uses CNI plugins to configure the network namespace used to secure the Consul service mesh sidecar proxy. All Nomad client nodes using network namespaces must have CNI plugins installed.

The following commands install CNI plugins:

```
curl -L -o cni-plugins.tgz "https://github.com/containernetworking/plugins/releases/download/v1.0.0/cni-plugins-linux-$( [ $(uname -m) = aarch64 ] && echo arm64 || echo amd64)"-v1.0.0.tgz
sudo mkdir -p /opt/cni/bin
sudo tar -C /opt/cni/bin -xzf cni-plugins.tgz
```

Ensure the your Linux operating system distribution has been configured to allow container traffic through the bridge network to be routed via iptables. These tunables can be set as follows:

```
echo 1 | sudo tee /proc/sys/net/bridge/bridge-nf-call-arptables
echo 1 | sudo tee /proc/sys/net/bridge/bridge-nf-call-ip6tables
echo 1 | sudo tee /proc/sys/net/bridge/bridge-nf-call-iptables
```

To preserve these settings on startup of a client node, add a file including the following to /etc/sysctl.d/ or remove the file your Linux distribution puts in that directory.
```
net.bridge.bridge-nf-call-arptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
```
## Configure systemd
Create a Nomad  service file at /etc/systemd/system/nomad.service.
```
sudo touch /etc/systemd/system/nomad.service
```
```
[Unit]
Description=Nomad
Documentation=https://www.nomadproject.io/docs/
Wants=network-online.target
After=network-online.target

# When using Nomad with Consul it is not necessary to start Consul first. These
# lines start Consul before Nomad as an optimization to avoid Nomad logging
# that Consul is unavailable at startup.
#Wants=consul.service
#After=consul.service

[Service]

# Nomad server should be run as the nomad user. Nomad clients
# should be run as root
User=nomad
Group=nomad

ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/usr/local/bin/nomad agent -config /etc/nomad.d
KillMode=process
KillSignal=SIGINT
LimitNOFILE=65536
LimitNPROC=infinity
Restart=on-failure
RestartSec=2

## Configure unit start rate limiting. Units which are started more than
## *burst* times within an *interval* time span are not permitted to start any
## more. Use `StartLimitIntervalSec` or `StartLimitInterval` (depending on
## systemd version) to configure the checking interval and `StartLimitBurst`
## to configure how many starts per interval are allowed. The values in the
## commented lines are defaults.

# StartLimitBurst = 5

## StartLimitIntervalSec is used for systemd versions >= 230
# StartLimitIntervalSec = 10s

## StartLimitInterval is used for systemd versions < 230
# StartLimitInterval = 10s

TasksMax=infinity
OOMScoreAdjust=-1000

[Install]
WantedBy=multi-user.target
```
> :warning: **Check Service_User and Service_Group for permissions errors**

Enable and start nomad with systemctl:
```
sudo systemctl enable nomad
```
```
sudo systemctl start nomad
```
```
sudo systemctl status nomad
```
## Setup Nomad environment variables to communicate with CLI
```
export NOMAD_ADDR=https://publicip:4646
export NOMAD_CACERT=nomad-ca.pem
export NOMAD_CLIENT_CERT=cli.pem
export NOMAD_CLIENT_KEY=cli-key.pem
```
## Add certificate to web browser to use Nomad's UI
Generate a client certificate for your web brower.

Then convert it to PKCS12 format.

```
openssl pkcs12 -export -inkey ./cli.key -in ./cli.pem -out ./cli.p12
```

For example, using Chrome:

Go to chrome://settings/certificates?search=certificate and import the converted certificate nomad-cli.p12.

# Nomad Client with GPU
Add nvidia plugin block to `etc/nomad.d/client.hcl` configuration:
```
plugin "nomad-device-nvidia" {
  config {
    enabled            = true
    fingerprint_period = "1m"
  }
}
```
Install  external Nvidia device plugin into the client plugin_dir (default = `/opt/nomad/plugin_dir`)
Follow the NVIDIA Container Toolkit installation instructions from Nvidia (https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html
) to prepare a machine to use docker containers with Nvidia GPUs. You should be able to run this simple command to test your environment and produce meaningful output.
```
docker run --rm --gpus all nvidia/cuda:11.0-base nvidia-smi
```
## NOMAD REQUIRED PORTS AND REQUIREMENTS:
https://www.nomadproject.io/docs/install/production/requirements
## NOMAD ARCHITECTURE
https://www.nomadproject.io/docs/concepts/architecture
## NOMAD GLOSSARY
https://learn.hashicorp.com/tutorials/nomad/get-started-vocab?in=nomad/get-started
## NOMAD NVIDIA GPU
https://developer.hashicorp.com/nomad/plugins/devices/nvidia
## NOMAD OPENSTACK VOLUME
https://github.com/hashicorp/nomad/tree/main/demo/csi/cinder-csi-plugin